package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String CLASS_NAME = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(CLASS_NAME, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(CLASS_NAME, "#### onStart ####");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(CLASS_NAME, "#### onResume ####");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(CLASS_NAME, "#### onPause ####");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(CLASS_NAME, "#### onStop ####");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(CLASS_NAME, "#### Destroyed ####");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(CLASS_NAME, "#### onRestart ####");
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }

}